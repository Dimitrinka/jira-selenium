**Instructions**
1. Clone repository
2. Open `TestAutomationFramework[test-automation-framework]` as a IntelliJ IDEA Project

Pre-conditions
As a tester you should put your credential in LoginPage class on 22 and 23 rows.
After that in config.properties on jiraProjectPage put your projectUrl.
In LinkBugToStoryPage class on 17 row - project url/browse/storyKey   Ex:"https://a38-first-jira-project.atlassian.net/browse/JVAP-17
In this class on the 30 row put your bug key and on the 43 row replace "NAME" with your bug name


