package pages.trello;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends BaseTrelloPage {

    public static String boardId = null;

    public LoginPage(WebDriver driver) {
        super(driver, "trello.loginPage");
    }

    public void loginUser(String userKey) {
        String username = getConfigPropertyByKey("trello.users." + userKey + ".username");
        String password = getConfigPropertyByKey("trello.users." + userKey + ".password");

        navigateToPage();
        assertPageNavigated();

        actions.waitForElementVisible("jira.projectPage.username");

        actions.typeValueInField(username, "jira.projectPage.username");
        actions.waitForElementVisible("jira.projectPage.loginButton");
        actions.clickElement("jira.projectPage.loginButton");

        actions.waitForElementClickable("jira.projectPage.loginSubmitButton");
        actions.waitForElementClickable("jira.projectPage.password");

        actions.typeValueInField(password, "jira.projectPage.password");
        actions.clickElement("jira.projectPage.loginSubmitButton");

        actions.waitForElementVisible("jira.header.member.menuButton");
    }
}
