package pages.jira;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class LinkBugToStoryPage extends ProjectJiraPage{
    public LinkBugToStoryPage(WebDriver driver) {
        super(driver, "jira.projectPage");
    }

    public void linkBugToStory() {
        actions.waitForElementClickable("jira.backlogButton");
        actions.clickElement("jira.backlogButton");
        actions.waitFor(2000);
        driver.navigate().to("https://a38-first-jira-project.atlassian.net/browse/JVAP-17");
        actions.waitFor(2000);
        actions.waitForElementClickable("jira.linkButton");
        actions.clickElement("jira.linkButton");
        actions.waitForElementClickable("jira.linkIssue.dropDown");
        actions.clickElement("jira.linkIssue.dropDown");
        actions.waitFor(2000);

        actions.clickElement("jira.linkIssue.isBlockedBy");
        actions.waitFor(2000);

        actions.waitForElementVisible("jira.linkIssueSearch");

        String bugKey="JVAP-14";
        actions.typeValueInField(bugKey,"jira.linkIssueSearch");
        actions.pressKey(Keys.ENTER);
        actions.waitFor(2000);
        actions.waitForElementClickable("jira.link.finalButton");
        actions.clickElement("jira.link.finalButton");

    }


    public void validateLinkedIssues() {

        actions.waitFor(3000);
        Assert.assertTrue(actions.getDriver().findElement(By.xpath("//span[contains(text(),'NAME')]"))
                .isDisplayed());

    }

}
