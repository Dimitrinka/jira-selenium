package pages.jira;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getUIMappingByKey;

public class CreateStoriesPage extends ProjectJiraPage{
    public CreateStoriesPage(WebDriver driver)
    {
        super(driver,"jira.projectPage");
    }

    public void createStory() {
        String storySummary = getUIMappingByKey("jira.storyName");
        //jira.header.createStory.dropDownButton

        actions.waitForElementClickable("jira.header.create.menuButton");
        actions.clickElement("jira.header.create.menuButton");
        actions.waitForElementClickable("jira.header.dropDown.dropDownButton");
        actions.clickElement("jira.header.dropDown.dropDownButton");
        actions.waitFor(2000);

        actions.clickElement("jira.header.createStory.dropDownButton");
        actions.waitFor(2000);
        actions.waitForElementClickable("jira.create.summary");
        actions.typeValueInField(storySummary, "jira.create.summary");
        String storyDescription =getUIMappingByKey("jira.storyDesc");
        actions.waitFor(2000);
        actions.waitForElementClickable("//div[@role='textbox']");
        actions.typeValueInField(storyDescription, "//div[@role='textbox']");

        actions.waitForElementClickable("jira.createStory.priority.dropDownButton");
        actions.clickElement("jira.createStory.priority.dropDownButton");
        actions.waitForElementClickable("jira.createStory.dropDown.mediumPriority");
        actions.clickElement("jira.createStory.dropDown.mediumPriority");
        actions.waitForElementClickable("jira.create.submitButton");
        actions.clickElement("jira.create.submitButton");
    }
    public void assertSearchInputVisible() {
        actions.assertElementPresent("jira.message.popup");
    }
}
