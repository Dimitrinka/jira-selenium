package pages.jira;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getUIMappingByKey;

public class CreateBugPage extends ProjectJiraPage{
    public CreateBugPage(WebDriver driver) {
        super(driver, "jira.projectPage");
    }
    public static final String NAME="Do not display correct page after click How To Book";

    public void createBug() {
        //jira.createBud.desc
        //jira.createBug.name

        String bugName = getUIMappingByKey(NAME);
        actions.waitForElementClickable("jira.header.create.menuButton");
        actions.clickElement("jira.header.create.menuButton");
        actions.waitForElementClickable("jira.header.dropDown.dropDownButton");
        actions.clickElement("jira.header.dropDown.dropDownButton");
        actions.waitFor(2000);

        actions.clickElement("jira.createBug.dropDownButton");
        actions.waitFor(2000);
        actions.waitForElementClickable("jira.create.summary");
        actions.typeValueInField(bugName, "jira.create.summary");
        String bugDescription =getUIMappingByKey("jira.createBug.desc");
        actions.waitFor(2000);
        actions.waitForElementClickable("jira.descInput");
        actions.typeValueInField(bugDescription, "jira.descInput");

        actions.waitForElementClickable("jira.createStory.priority.dropDownButton");
        actions.clickElement("jira.createStory.priority.dropDownButton");
        actions.waitForElementClickable("jira.createStory.dropDown.mediumPriority");
        actions.clickElement("jira.createStory.dropDown.mediumPriority");
        actions.waitForElementClickable("jira.create.submitButton");
        actions.clickElement("jira.create.submitButton");
    }
    public void assertSearchInputVisible() {
        actions.assertElementPresent("jira.message.popup");
    }
}
