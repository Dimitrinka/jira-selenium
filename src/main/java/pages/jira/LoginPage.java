package pages.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends ProjectJiraPage{


    public LoginPage(WebDriver driver) {
        super(driver, "jira.projectPage");
    }

    public void loginUser() {

        String username = "";
        String password = "";

        actions.waitForElementVisible("jira.loginPage.username");
        actions.typeValueInField(username, "jira.loginPage.username");
        actions.clickElement("jira.loginPage.loginButton");
        actions.waitFor(2000);
        actions.waitForElementVisible("jira.loginPage.password");
        actions.typeValueInField(password, "jira.loginPage.password");
        actions.clickElement("jira.loginPage.loginButton");

    }
}
