package pages.jira;

import org.openqa.selenium.WebDriver;
import com.telerikacademy.testframework.pages.BasePage;


public abstract class ProjectJiraPage extends BasePage {
    public ProjectJiraPage(WebDriver driver, String pageUrlKey) {
        super(driver, pageUrlKey);
    }
}
