package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.jira.LoginPage;


public class ProjectJiraTest {
    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("jira.projectPage");
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }

}
