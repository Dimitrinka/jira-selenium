package test.cases.jira;

import org.junit.Test;

import pages.jira.LinkBugToStoryPage;
import pages.jira.LoginPage;

public class LinkBugToStoryTest extends ProjectJiraTest {
    @Test
    public void linkBugToStory() {

        LoginPage log = new LoginPage(actions.getDriver());
        log.loginUser();
        LinkBugToStoryPage issuePage = new LinkBugToStoryPage(actions.getDriver());

        issuePage.linkBugToStory();
        issuePage.validateLinkedIssues();


    }
}
