package test.cases.jira;

import org.junit.Test;

import pages.jira.CreateBugPage;
import pages.jira.LoginPage;

public class CreateBugTest extends ProjectJiraTest {
    @Test
    public void createBugWhenCreateBugClicked() {

        LoginPage log = new LoginPage(actions.getDriver());
        log.navigateToPage();
        log.loginUser();

        CreateBugPage bugPage = new CreateBugPage(actions.getDriver());
        bugPage.createBug();
        bugPage.assertSearchInputVisible();
    }
}
