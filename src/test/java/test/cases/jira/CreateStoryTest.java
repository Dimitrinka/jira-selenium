package test.cases.jira;

import org.junit.Test;
import pages.jira.CreateStoriesPage;
import pages.jira.LoginPage;


public class CreateStoryTest extends ProjectJiraTest{
    @Test
    public void createStoryWhenCreateStoryClicked() {

        LoginPage log = new LoginPage(actions.getDriver());
        log.navigateToPage();
        log.loginUser();

        CreateStoriesPage storiesPage = new CreateStoriesPage(actions.getDriver());
        storiesPage.createStory();
        storiesPage.assertSearchInputVisible();
    }
}
